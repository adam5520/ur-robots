from kivy.app import App
from kivy.utils import platform
from kivymd.uix.dialog import MDDialog

class NavigationHelper():
    has_centered_map = False
    def run(self):
        #Request access from android

        if platform == 'android':
            from android.permissions import Permission, request_permissions
            def callback(permission, resuslts):
                if all([res for res in resuslts]):
                    print("Permissions Gained")
                else:
                    print("Missing some permissions")


            request_permissions([Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION], callback)

        #Start blinking the GPS marker

        gps_blinker = App.get_running_app().root.ids.mapview.ids.blinker
        gps_blinker.blink()

        #Configuring GPS
        if platform == 'android':
            from plyer import gps
            gps.configure(on_location=self.update_blinker_position,
                          on_status=self.on_auth_status)

            gps.start(minTime=1000, minDistance=0)

    def update_blinker_position(self, *args, **kwargs):
        my_lat = kwargs['lat']
        my_lon = kwargs['lon']
        print("GPS POSITION", my_lat, my_lon)
        #Keep updating gps position
        gps_blinker = App.get_running_app().root.ids.mapview.ids.blinker
        gps_blinker.lat = my_lat
        gps_blinker.lon = my_lon


        #Center map
        if not self.has_centered_map:
            map = App.get_running_app().root.id.mapview
            map.center_on(my_lat, my_lon)
            self.has_centered_map = True



    def on_auth_status(self, general_status, general_info):
        if general_status == 'provider-enabled':
            pass
        else:
            self.open_gps_access_popup()

    def open_gps_access_popup(self):
        dialog = MDDialog(title= "Gps ERROR", text="You need to turn on GPS access for the app to function")
        dialog.size_hint = [.8,.8]
        dialog.pos_hint = {'center_x': .5, 'center_y': .5}
        dialog.open()





        pass