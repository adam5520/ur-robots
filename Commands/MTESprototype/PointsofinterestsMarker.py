from kivy_garden.mapview import MapMarkerPopup
from popupmenu import LocationPopupMenu

class PoiMarker(MapMarkerPopup):
    source = "206317.png"
    poi_data =[]


    def on_release(self):
        # Open up the location popupmenu
        menu = LocationPopupMenu(self.market_data)
        menu.size_hint = [ ]
        menu.open()


