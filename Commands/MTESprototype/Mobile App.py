from kivymd.app import MDApp
from mountainmapview import MountainMapView
import sqlite3
from navigationhelper import NavigationHelper

class MainApp(MDApp):
    connection = None
    cursor = None


    def on_start(self):
        self.theme_cls.primary_palette = 'BlueGray'
        # Start gps
        NavigationHelper().run()

        #Connect to database
        self.connection = sqlite3.connect("Threat_data.db")
        self.cursor = self.connection.cursor()



MainApp().run()