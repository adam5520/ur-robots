
from kivy_garden.mapview import MapView
from kivy.clock import Clock
from kivy.app import App
from PointsofinterestsMarker import PoiMarker


#poi points of interest
#fov field of view
class MountainMapView(MapView):
    getting_poi_timer = None
    poi_names = []


    def start_getting_poi_in_fov(self):
        # After one seconds, get the poi in the field of view
        try:
            self.getting_poi_timer.cancel()
        except:
            pass

        self.getting_poi_timer = Clock.schedule_once(self.get_poi_in_fov, 1 )

    def get_poi_in_fov(self, *args):
        print(self.get_bbox())
        #Get reference to mobile app and the data cursor
        min_lat, min_lon, max_lat, max_lon = self.get_bbox()
        app = App.get_running_app()
        sql_statement = "SELECT * FROM Threat_data WHERE x > %s AND x < %s AND y > %s AND y < %s "%(min_lon, max_lon, min_lat, max_lat)
        app.cursor.execute(sql_statement)
        pois = app.cursor.fetchall()
        print(pois)
        for poi in pois:
            name = poi[1]
            if name in self.poi_names:
                continue
            else:
                self.add_poi(poi)




    def add_poi(self, poi):
        #Create poiMarker
        lat, lon = poi[15], poi[20]
        marker = PoiMarker(lat=lat, lon=lon)
        marker.poi_data = poi

        #Add poiMarker to the map
        self.add_widged(marker)

        #Keep track of the poi name
        name = poi[1]
        self.poi_names.append(name)
