from kivy_garden.mapview import MapMarker
from kivy.animation import Animation

class GpsBlinker(MapMarker):
    def blink(self):
        # Animation that changes blinking size
        ani = Animation(opacity=0, blink_size=50)
        ani.bind(on_complete=self.reset)
        ani.start(self)


    def reset(self, *args):
        self.opacity = 1
        self.blink_size = self.default_blink_size
        self.blink()

        # Create loop after finishing animation to create new one

