from kivymd.uix.dialog import ListMDDialog

class LocationPopupMenu(ListMDDialog):
    def __init__(self, poi_data):
        super()._init_()

        super()._init_()
        # Ser all of the fields of poi data
        headers = "ID,Threat,Timereported,x,y,Location,Description"
        headers = headers.split(',')


        for i in range(len(headers)):
            attribute_name = headers[i]
            attribute_value = poi_data[i]
            setattr(self, attribute_name, attribute_value)


